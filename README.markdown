augeas-datadogagent
-------------------

Augeas lens for the Datadog monitoring agent config file. The file format is INI-file
except with colon(:) as separator instead of '='.
